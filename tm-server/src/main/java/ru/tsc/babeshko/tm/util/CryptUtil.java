package ru.tsc.babeshko.tm.util;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;

public interface CryptUtil {

    @NotNull
    String TYPE = "AES/ECB/PKCS5Padding";

    @NotNull
    String HASH_ALGORITHM = "SHA-1";

    @NotNull
    String CRYPTO_ALGORITHM = "AES";

    @NotNull
    Integer MAX_LENGTH_SECRET_KEY = 16;


    @NotNull
    @SneakyThrows
    static SecretKeySpec getKey(@NotNull final String secretKey) {
        @NotNull final MessageDigest sha = MessageDigest.getInstance(HASH_ALGORITHM);
        @NotNull final byte[] key = secretKey.getBytes(StandardCharsets.UTF_8);
        @NotNull final byte[] digest = sha.digest(key);
        @NotNull final byte[] secret = Arrays.copyOf(digest, MAX_LENGTH_SECRET_KEY);
        return new SecretKeySpec(secret, CRYPTO_ALGORITHM);
    }

    @NotNull
    @SneakyThrows
    static String encrypt(@NotNull String secret, @NotNull String strToEncrypt) {
        @NotNull final SecretKeySpec secretKey = getKey(secret);
        @NotNull final Cipher cipher = Cipher.getInstance(TYPE);
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        @NotNull final byte[] bytes = strToEncrypt.getBytes(StandardCharsets.UTF_8);
        return Base64.getEncoder().encodeToString(cipher.doFinal(bytes));
    }

    @NotNull
    @SneakyThrows
    static String decrypt(@NotNull String secret, @NotNull String strToDecrypt) {
        @NotNull final SecretKeySpec secretKey = getKey(secret);
        @NotNull final Cipher cipher = Cipher.getInstance(TYPE);
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
    }

}