package ru.tsc.babeshko.tm.exception.field;

import ru.tsc.babeshko.tm.exception.AbstractException;

public final class IncorrectIndexException extends AbstractException {

    public IncorrectIndexException() {
        super("Error! Incorrect index...");
    }

}