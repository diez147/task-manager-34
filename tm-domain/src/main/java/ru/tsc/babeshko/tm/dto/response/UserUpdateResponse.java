package ru.tsc.babeshko.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.model.User;

@NoArgsConstructor
public final class UserUpdateResponse extends AbstractUserResponse {

    public UserUpdateResponse(@Nullable final User user) {
        super(user);
    }

}