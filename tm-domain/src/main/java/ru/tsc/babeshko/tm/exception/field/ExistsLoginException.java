package ru.tsc.babeshko.tm.exception.field;

import ru.tsc.babeshko.tm.exception.AbstractException;

public final class ExistsLoginException extends AbstractException {

    public ExistsLoginException() {
        super("Error! Login already exists...");
    }

}